#include <stdio.h>

// Time: Linear O(n)
// Memory: Constant O(1)
int fib(int n)
{
    if (n <= 0)
        return 0;
    
    if (n == 1 || n == 2)
        return 1;
    
    int a = 1, // k - 2 th number
        b = 1, // k - 1 th number
        c; // kth number
    
    for (int i = 3; i <= n; i++)
    {
        c = a + b;
        a = b;
        b = c;
    }
    
    return c;
}

int main()
{
    printf("%d\n", fib(10));
}
