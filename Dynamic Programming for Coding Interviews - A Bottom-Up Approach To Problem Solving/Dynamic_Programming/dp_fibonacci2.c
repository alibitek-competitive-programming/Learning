#include <stdio.h>

// Time: Linear O(n)
// Memory: Linear O(n)
int fib(int n)
{
    if (n <= 0)
        return 0;
    
    if (n == 1 || n == 2)
        return 1;
    
    int memo[n];
    
    memo[1] = 1; // k - 2 th number
    memo[2] = 1; // k - 1 th number
    
    for (int i = 3; i <= n; i++)
    {
        // kth term
        memo[i] = memo[i - 1] + memo[i - 2];
    }
    
    return memo[n];
}

int main()
{
    printf("%d\n", fib(10));
}
