#include <stdio.h>

#define N 4

// Lets assume there are four stations (0 to 3)
// Cost of tickets (e.g. cost[i][j] = cost of ticket from station i to station j)
int cost[N][N] =
{
    { 0, 10, 75, 94 },
    { -1, 0, 35, 50 },
    { -1, -1, 0, 80 },
    { -1, -1, -1, 0 }
};

int memo[N][N] = { 0 };

/**
 * There are N station in a route, starting from 0 to N - 1.
 * A train moves from first station (0) to last station (N - 1) in only forward direction.
 * The cost of ticket between any two staitons is given.
 * Find the minimum cost of travel from station 0 to station N - 1
 * 
 * Time: O(n^2)
 * Memory: O(n)
 */
int calculateMinCost(int source, int destination)
{
    // minCost[i] = min cost from station 0 to station i
    int minCost[N];
    minCost[0] = 0;
    minCost[1] = cost[0][1];
   
    for (int i = 2; i < destination; i++)
    {
        // cost of going directly from source current station
        minCost[i] = cost[0][i];
        
        // cost of going indirectly via other stations from source to current station
        for (int j = 1; j < i; j++)
        {
            if (minCost[i] > minCost[j] + cost[i][j])
                minCost[i] = minCost[j] + cost[i][j];
        }
    }
    
    return minCost[destination - 1];
}

int main()
{
    printf("minCost=%d\n", calculateMinCost(0, 3));
}
