#include <stdio.h>

#define N 20

int memo[N] = { 0 };

// Memoization (caching) optimises an exponential time function O(2^n) to take linear time O(n)
int fib(int n)
{
    if (n <= 0)
        return 0;
    
    if (memo[n] != 0)
        return memo[n];
    
    if (n == 1 || n == 2)
        memo[n] = 1;
    else
        memo[n] = fib(n - 1) + fib(n - 2);
    
    return memo[n];
}

int main()
{
    printf("fib(%d)=%d\n", N, fib(N));
}
