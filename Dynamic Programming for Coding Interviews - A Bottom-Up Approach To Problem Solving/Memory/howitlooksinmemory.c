
int g = 5; // goes in Data area

int main()
{
    static int a; // 0 default initialized, goes in Data area
    
    // Stack (on Activation Record of main, default value is garbage)
    int b;
    int *p; 
    
    // allocated on heap
    p = (int*) malloc(sizeof(int));
    *p = 10;
}
