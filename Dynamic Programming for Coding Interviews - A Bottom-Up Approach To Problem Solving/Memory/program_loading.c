#include <stdio.h>

// Go in data area at load time. Initialized with 0
int total;

/**
 * Code (machine instructions) of function goes in code area.
 * When this function is called, then Activation Record of the function is created on the Stack
 */
int square(int x)
{
    // x goes in AR when this function is called
    return x * x;
}

/**
 * Code of function goes in code area.
 * When this function is called (at run-time), its AR gets created on Stack and memory to non-static local variables (x, y) is allocated in that AR.
 * count, being a static variable, is allocated in data area at load time.
 */
int squareOfSum(int x, int y)
{
    static int count = 0; // load-time variable
    printf("Fun called %d times", ++count);
    return square(x + y);
}

/**
 * Code goes in code area.
 * When main is called, its activation record gets created on Stack and memory to non-static local variables (a and b) is allocated in that Activation Record
 */
int main()
{
    int a = 4, b = 2;
    total = squareOfSum(a, b);
    printf("Square of sum = %d", total);
}
