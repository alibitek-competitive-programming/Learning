#include <stdio.h>

int fibRecursive(int n)
{
    if (n <= 0)
        return 0;
    
    if (n == 1 || n == 2)
        return 1;
    
    return fibRecursive(n - 1) + fibRecursive(n - 2);
}

int fibIterative(int n)
{
    int a = 1, b = 1, c, cnt = 3;
    
    if (n == 1 || n == 2)
        return 1;
    
    for (cnt = 3; cnt <= n; cnt++)
    {
        c = a + b;
        a = b;
        b = c;
    }
    
    return c;
}

int main()
{
    printf("%d\n", fibIterative(10));
}
