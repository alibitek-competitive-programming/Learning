#include <stdio.h>

#define N 4

// Lets assume there are four stations (0 to 3)
// Cost of tickets (e.g. cost[i][j] = cost of ticket from station i to station j)
int cost[N][N] =
{
    { 0, 10, 75, 94 },
    { -1, 0, 35, 50 },
    { -1, -1, 0, 80 },
    { -1, -1, -1, 0 }
};

/**
 * There are N stations in a route, starting from 0 to N - 1.
 * A train moves from first station (0) to last station (N - 1) in only forward direction.
 * The cost of ticket between any two staitons is given.
 * Find the minimum cost of travel from station 0 to station N - 1
 */
int calculateMinCost(int source, int destination)
{
    if (source == destination // both stations are the same
        || source == destination - 1) // when source is just before destination, then there is only one way to reach destination from source
        return cost[source][destination];
    
    int minCost = cost[source][destination];
    
    // Optimal substructure property because we are computing the min cost of travel between intermediate stations to find the actual min cost of going from initial source to final destination
    for (int i = source + 1; i < destination; i++)
    {
        int temp = calculateMinCost(source, i) + calculateMinCost(i, destination);
        
        if (temp < minCost)
            minCost = temp;
    }
    
    return minCost;
}

int main()
{
    printf("minCost=%d\n", calculateMinCost(0, N - 1));
}
