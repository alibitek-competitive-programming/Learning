# Dynamic Programming for Coding Interviews: A Bottom-Up Approach To Problem Solving

## Recursion
- A recursive function typically performs some task and calls itself.  
- If the recursive call is made before the function performs its own task, then it is called head recursion.  
- If the recursive call is made at the end, then it is called Tail Recursion.  
- Most of the times (esp. in case of dynamic porgraming problems) recursion is more complex and may not be simple head or tail recursion.  

- How to solve a problem using recursion  
Our focus is to solve a problem, with recursion, we can actually code without solving a problem, if we can somehow define the large problem in terms of smaller problems of same type.

- Our focus is on solving the problem for top case and leave the rest for recursive calls to do.  
- We cannot be good coders without mastering the art of recursion.  
My suggestion to everyone reading this book is that when you write simple programs like linear search, binary search, sorting, etc. try to also implement them recursively.
- Recursive functions are very difficult to expand inline because compiler 

## Memory layout as a problem solving tool
- A function call is a lot of overhead both in terms of time and memory
- Some compilers optimize performance by replacing function call with entire code of the function during compilation, hence avoiding the actual function call overheads.
- Recursive functions are very difficult to expand inline because the compiler may not know the depth of function call at compile time
- One of the reasons behind the popularity of macros in C language is this overhead in function call.  
Another was the type independence that macros bring.
- In C++, both benefits are given in the form of inline functions and templates and they are not error prone like macros.
- In recursive version, one activation record (AR) is created on the stack for each value of n. If n=1000, then 1000 ARs are created.
Therefore, the extra memory taken is O(n).
- Recursion is a huge overhead. Both in terms of memory and execution time.
- One of the major problem with recursive functions comes when recursive calls starts overlapping at the level of subproblems.
- Conclusion:
    - A function will have multiple ARs inside stack if and only if it is recursive
    - Global and static variables can only be initialized with constants
    - The memory to load-time variables is allocated before any function is called
    - The memory to load-time variables is released only after the execution is complete

## Optimal substructure
- Optimal substructure means, that optimal solution to a problem of size n (having n elements) is based on an optimal solution to the same problem of smaller size (less than n elements)
- That is, while building the solution for a problem of size n, define it in terms of similar problems of smaller size, say k, (with k < n)
- We find optimal solutions of less elements and combine the solutions to get final result
- In a nutshell, it means, we can write recursive formula for a solution to the problem of finding shortest path
- The problem of finding the shortest route between two cities demonstrates optimal substructure property.
- This is one of the two conditions of dynamic programming. Another is overlapping subproblems.
- Standard algorithms like Floyd-Warshall and Bellman-Ford to find all-pair shortest paths are typical examples of dynamic programming.

### Use of optimal substructure in dynamic programming
- Fundamentally, dynamic programming is an important tool for optimizing recursive solutions in a way that is more efficient, both in terms of memory and time than regular recursion.
- Writing a recursive formula or defining the optimal substructure is the first step toward dynamic programming.
- If we cannot write a recursive formula for the problem, we may not be thinking aout using dynamic programming.
- The logic of dynamic programming usually comes from recursion. Solution of a problem is derived from solution of subproblems, solution of subproblem is derived from solution of sub-subproblems and so on.
- In questions of dynamic programming, it is a good idea, to first solve the problem using recursive formula and then use the same formula with bottom-up approach of dynamic programming.

## Overlapping subproblems
- Singular recursive problem: each subproblem is solved only once
- Complex recursive problem: each subproblem is solved multiple times. (Each recursive function is called with exactly the same parameters more than once)
- Memoization, dynamic programming and greedy approach are techniques used to solve the problem of solving each subproblem only once

## Memoization
- In memoization we store the solution of a subproblem in some sort of a cache when it s solved fro the first time.
- When the same subproblem is encountered again, then the problem is not soled from scratch, rather, it's already solved result is returned from the cache.
- Deciding data structure of cache is an important step in memoization. The cache should be capable of storing results of all subproblems. Usually cahce is an array. If our problem has only one dimension, then it is a one-dim array, else use multi-dimensional array.
- Memoization is a strong technique, it improves the performance in a big way by avoiding multiple re-computations of subproblems.
- The goodness of recursion (to be able to visualize a problem and solve it in a top-down fashion) is used without the side effects of overlapping subproblems that comes with recursion.
- Memoization = Recursion + Cache - Recomputing overlapping subproblems
- But, even without overlapping subproblems, recursion itself is an overhead because multiple instances of Action Record gets created in the stack (and each AR creation and removal is a cost in terms of both memory and time). And because memoization uses recursion, there is still room for improvement.
- In fact, if there are no overlapping subproblems then memoization will be exactly similar to recursion in terms of execution time.
- The dynamic programming approach is the bottom-up approach to problem solving that reduces both time and space complexity further.

## Dynamic programming
- Both memoization and dynamic programming solve individual subproblems only once
- The difference is that, memoization uses recursion and works top-down, whereas dynamic programming moves in oppositve direction solving the problem bottom-up
- Dynamic programming unrolls the recursion and moves in opposite direction to memoization
- Dynamic programming is the most optimal solution, in terms of both execution time and memory as seen in Fibonacci and min-distance problems
- Major applications of DP is in solving complex problems bottom-up where the problem has optimal substructure and subproblems overlap.
- The challenge with dynamic programming is that it is not always intuitive especially for complex problems.
- Sometimes the subproblems overlap in a non-obovious way and does not appear to have an intuitive recursive solution

## Top-Down v/s Bottom-Up
- Top-down:  
    - In top-down we have an understanding of the destination initially and we develop the means required to reach there
    - First you say I will take over the world. How will you do that? You say, I will take over Asia first. How will you do that? I will take over India first. How will you do that? I will first become the Chief Minister of Delhi, etc. etc.
- Bottom-up:
    - On the other hand, bottom-up has all the means available and we move toward the destination
    - You say, I will become the CM of Delhi. Then will take over India, then all other countries in Asia and finally I will take over the whole world
- Recursion is a top-down approach of problem solving
- Memoization is also top-down, but it is an improvement over recursion where we cache the results when a subproblem is solved.  
When the same subproblem is encountered again we use the result from cahe rather than computing it again.
It has the drawbacks of recursion with an improvement that each problem is solved only once
- If there are no overlapping subproblems (e.g. factorial) memoized function will be exactly the same as recursive function.
- Top-down is usually more intuitive because we get a bird's eye view and a broader understanding of the solution
- Dynamic programming attempts to solve the problem in a bottom-up manner avoiding the overhead of recursion altogether
- In almost all cases, bottom-up is better than top-down
### Negatives of bottom-up dynamic programming
    - In top-down approach (recursion or memoized) we do not solve all the subproblems, we only solve those problems that need to be solved to get the solution of main problem.
    - In bottom-up dynamic programming, all the subproblems are solved before getting to the main problem
    - Example: In combinatorics, combination is defined recursively: C(n, m) = C(n - 1, m) + C(n - 1, m - 1)  
        - The dynamic programming solution for this problem requires to construct the entire Pascal triangle and return (m+1)th value in the (n+1)th row
        - The recursive solution on the other hand computes only the required nodes of Pascal triangle
        - If n and m are very big values then recursion may actually beat dynamic programming, both in terms of time and memory