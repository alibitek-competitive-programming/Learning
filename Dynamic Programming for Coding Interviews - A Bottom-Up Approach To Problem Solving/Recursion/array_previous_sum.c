#include <stdio.h>
#include <stddef.h>

/*
 Give an array, arr, of integers, write a recursive function that add sum of all the previous numbers to each index of the array
 */

int sum(int* arr, unsigned int index)
{
    if (index == 0)
        return arr[0];
    
    return arr[index] = arr[index] + sum(arr, index - 1);
}

int main()
{
    int arr[6] = { 1, 2, 3, 4, 5, 6 };
    
    size_t size = sizeof(arr) / sizeof(arr[0]);
    sum(arr, size - 1);
    
    for (unsigned int i = 0; i < size; i++)
        printf("arr[%u]=%d\n", i, arr[i]);
}
