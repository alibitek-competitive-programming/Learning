#include <stdio.h>
#include <stddef.h>

/* 1 <~> 2
 1)
 0001
 0010
 XOR = 0011 (a) == 3
       0010 (b) == 2
 => 2) 0001 (b) == 1
       0011 (a) == 3
 => 3) 0010 (a) == 2

1) A = A XOR B
2) B = B XOR A
3) A = A XOR B
*/
void swap(int *a, int *b)
{
    *a ^= *b;
    *b ^= *a;
    *a ^= *b;
}

/*
n = 3
9 6 2
0 1 2

n - 1 = 2

i = 0
    j = 0 .. 3 - 0 - 1 (2)
    arr[0] > arr[1] => 6 9 2
    j = 1
    arr[1] > arr[2] => 6 2 9
    j = 2 < 2 (F)

i = 1
    j = 0 .. 3 - 1 - 1 (1)
    arr[0] > arr[1] => 2 6 9
    j = 1 1 < 1 (F)

i = 2 < 2 (F)
    stop

 * After traversing for the first time, the largest element reaches last position in the array
 * In the second pass, the second largest element reaches second last position and so on.
 */
void bubbleSort(int *arr, int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - i - 1; j++)
        {
            if (arr[j] > arr[j+1])
            {
                swap(&arr[j], &arr[j+1]);
            }
        }
    }
}

void bubbleSortRec(int* arr, int n)
{
    if (n == 0 || n == 1)
        return;
    
    // Perform one pass
    for (int j = 0; j < n - 1; j++)
    {
        if (arr[j] > arr[j + 1])
        {
            swap(&arr[j], &arr[j + 1]);
        }
    }
    
    // Leave rest to recursion
    bubbleSortRec(arr, n - 1);
}

void printArray(int *arr, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d\n", arr[i]);
    }
}

int main()
{
    int arr[8] = { 9, 6, 2, 12, 11, 9, 3, 7 };
    size_t size = sizeof(arr) / sizeof(arr[0]);
    bubbleSortRec(arr, size);
    printArray(arr, size);
    
}
