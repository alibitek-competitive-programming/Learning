#include <stdio.h>

int factorial(unsigned int n)
{
    if (n == 0 || n == 1)
        return n;
    
    return n * factorial(n - 1);
}

int factorial_it(unsigned int n)
{
    if (n == 0 || n == 1)
        return n;
    
    int result = 1;
    
    for (unsigned int i = 2; i <= n; i++)
        result *= i;
    
    return result;
}

int factorial_squared(unsigned int n)
{
    return factorial(n * n);
}

int main()
{
    printf("%d = %d; %d\n", factorial(6), factorial_it(6), factorial_squared(3));
}
