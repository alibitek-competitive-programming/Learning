#include <stdio.h>
#include <stdlib.h>

typedef 
    struct Node
    {
        int data;
        struct Node* next;
    }
Node;

/**
 * Head recursion
 * First traverse rest of the list, then print value at current Node
 */
void traverse_head(Node* head)
{
    if (head != NULL)
    {
        traverse_head(head->next);
        printf("%d\n", head->data);
    }
}

/**
 * Tail recursion
 * First print value at current Node, then traverse rest of the list
 */
void traverse_tail(Node* head)
{
    if (head != NULL)
    {
        printf("%d\n", head->data);
        traverse_tail(head->next);
    }
}

void cleanup(Node* head)
{
    if (head != NULL)
    {
        cleanup(head->next);
        free(head);
    }
}

int main()
{
    Node* head = NULL;
    
    Node* node1 = (Node*)malloc(sizeof(Node));
    node1->data = 1;
    
    head = node1;
    
    Node* node2 = (Node*)malloc(sizeof(Node));
    node2->data = 2;
    node1->next = node2;
    
    Node* node3 = (Node*)malloc(sizeof(Node));
    node3->data = 3;
    node2->next = node3;
    
    Node* node4 = (Node*)malloc(sizeof(Node));
    node4->data = 4;
    node3->next = node4;
    node4->next = NULL;
    
    traverse_head(head);
    printf("\n");
    traverse_tail(head);
    
    cleanup(head);
}
