#include <stdio.h>
#include <stdlib.h>

typedef 
    struct Node
    {
        struct Node* left;
        char data;
        struct Node* right;
    } 
Node;

void inOrder(Node* root)
{
    if (!root)
        return;
    
    if (root->left)
        inOrder(root->left);
    
    printf("%c\n", root->data);
    
    if (root->right)
        inOrder(root->right);
}

int main()
{
    Node* A = (Node*) malloc(sizeof(Node));
    Node* B = (Node*) malloc(sizeof(Node));
    Node* C = (Node*) malloc(sizeof(Node));
    Node* E = (Node*) malloc(sizeof(Node));
    Node* F = (Node*) malloc(sizeof(Node));
    Node* G = (Node*) malloc(sizeof(Node));
    
    A->data = 'A';
    A->left = B;
    A->right = C;
    
    B->data = 'B';
    B->left = E;
    
    C->data = 'C';
    C->left = F;
    C->right = G;
    
    E->data = 'E';
    F->data = 'F';
    G->data = 'G';
    
    inOrder(A);
    
    free(A);
    free(B);
    free(C);
    free(E);
    free(F);
    free(G);
}
