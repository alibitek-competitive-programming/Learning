#include <stdio.h>

void printTable(int n)
{
    for (int i = 1; i <= 10; i++)
    {
        printf("%d * %d = %d\n", n, i, (n * i));
    }
}

void printTableRecursive(int n, int i)
{
    if (i > 10)
        return;
    
    printf("%d * %d = %d\n", n, i, (n * i));
    printTableRecursive(n, i + 1);
}

int main()
{
    printTable(7);
    printf("\n==========\n");
    printTableRecursive(7, 1);
}
