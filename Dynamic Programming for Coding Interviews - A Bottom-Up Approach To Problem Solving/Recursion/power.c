#include <stdio.h>
#include <stddef.h>

int power(int x, int n)
{
    if (n == 0 || x == 1)
        return 1;
    
    return x * power(x, n - 1);
}

int main()
{
    int x = 2, n = 12;
    printf("power(%d, %d)=%d\n", x, n, power(x, n)); 
}
