#include <stdio.h>

int sum(unsigned int n)
{
    if (n == 0 || n == 1)
        return n;
    
    return n + sum(n - 1);
}

int sum_it(unsigned int n) 
{
    int sum = 0;
    
    for (int i = 1; i <= n; i++)
        sum += i;
    
    return sum;
}

int main()
{
    printf("%d\n", sum(5));
}
