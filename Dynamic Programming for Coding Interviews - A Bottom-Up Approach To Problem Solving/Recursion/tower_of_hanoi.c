#include <stdio.h>

/**
 * We have to move all the discs from source peg to destination peg.
 * With two restrictions:
 *  - Only one disc can be moved at a time
 *  - At any point in the process we should never place a larger disc on top of a smaller disc
 * 
 * If we think recursively, the problem can be solved in three simple steps.
 * 
 * @param s the source peg
 * @param d the destination peg
 * @param e the extra peg
 * @param n the number of discs (all initially in s)
 */
void towerOfHanoi(char s, char d, char e, unsigned int n)
{
    // terminating condition (no disc)
    if (n == 0)
        return;
    
    // Step-1: Move n - 1 discs from S to E using D
    towerOfHanoi(s, e, d, n - 1);
    
    // Step-2: Move the n'th disc from S to D
    printf("Move Disk-%d FROM %c TO %c\n", n, s, d);
    
    // Step-2: Move n - 1 discs from E to D using S
    towerOfHanoi(e, d, s, n - 1);
}

int main()
{
    towerOfHanoi('s', 'd', 'e', 3);
}
 
