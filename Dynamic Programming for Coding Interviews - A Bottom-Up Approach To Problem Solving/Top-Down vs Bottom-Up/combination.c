#include <stdio.h>

int combination(int n, int m)
{
    if (n == 0 || m == 0 || (n == m))
        return 1;
    
    return combination(n - 1, m) + combination(n - 1, m - 1);
}

int main()
{
    printf("C(5, 4)=%d\n", combination(5, 4));
}
