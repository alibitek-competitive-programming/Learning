#include <stdio.h>

/*
 * We start solving the problem from top factorial(n) and solves subproblems (at bottom) on need basis
 * If the solution of a subproblem is not required for computing the solution of a larger problem, the subproblem is not solved
 */
int factorialTopDown(unsigned int n)
{
    if (n == 0 || n == 1)
        return n;
    
    return n * factorialTopDown(n - 1);
}

/**
 * A bottom-up approach on the other hand develops the solution starting from the bottom
 * 1! = 1 = 1
 * 2! = 2 * (1!) = 2
 * 3! = 3 * (2!) = 6
 * 4! = 4 * (3!) = 24
 */
int factorialBottomUp(unsigned int n)
{
    if (n == 0 || n == 1)
        return n;
    
    int result = 1;
    
    for (unsigned int i = 2; i <= n; i++)
        result *= i;
    
    return result;
}

int main()
{
    printf("%d = %d\n", factorialTopDown(6), factorialBottomUp(6));
}
