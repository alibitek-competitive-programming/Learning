#include <stdio.h>
#include <stdlib.h>

// Given a binary tree, for each node add sum of all the nodes in its hierarchy to its value

typedef 
    struct Node
    {
        struct Node* left;
        int data;
        struct Node* right;
    } 
Node;

void preOrder(Node* root)
{
    if (!root)
        return;
    
    printf("%d\n", root->data);
    
    if (root->left)
        preOrder(root->left);
    
    if (root->right)
        preOrder(root->right);
}

void addChildSum(Node* root)
{
    if (root == NULL)
        return;
    
    if (root->left)
        addChildSum(root->left);
    
    if (root->right)
        addChildSum(root->right);
    
    int sum = root->data;
    
    if (root->left)
        sum += root->left->data;
    
    if (root->right)
        sum += root->right->data;
    
    root->data = sum;
}

int main()
{
    Node* A = (Node*) malloc(sizeof(Node));
    Node* B = (Node*) malloc(sizeof(Node));
    Node* C = (Node*) malloc(sizeof(Node));
    Node* D = (Node*) malloc(sizeof(Node));
    Node* E = (Node*) malloc(sizeof(Node));
    Node* F = (Node*) malloc(sizeof(Node));
    Node* G = (Node*) malloc(sizeof(Node));
    
    A->data = 2;
    A->left = B;
    A->right = C;
    
    B->data = 4;
    B->left = D;
    B->right = E;
    
    C->data = 1;
    C->right = F;
    
    D->data = 6;
    
    E->data = 9;
    E->left = G;
    
    F->data = 2;
    G->data = 3;
    
    addChildSum(A);
    preOrder(A);
    
    free(A);
    free(B);
    free(C);
    free(D);
    free(E);
    free(F);
    free(G);
}
